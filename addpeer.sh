#!/bin/bash
#Usage:
#
#addpeer.sh peername clientip
#
#where peername is a string and clientip is an ip address only ( ex 192.168.1.1)
#

configfile="wg0.conf"      # This is the name of the server's config file
pubkeyname="wg-public.key" # This is the name of the server's public key file
domainname="my.domain"     # Name of the domain (server's external address, IP address or DNS name)
port="49000"               # Server listening port number - UDP only, if the server is behind NAT it needs to be port forwarded
peername=$1                # A descriptive human readable peer name, it is taken from command line if supplied as first argument
clientip="$2"              # IP address of the client. Note that every client must have it's own distinct IP address
#dns="10.8.1.1"            # Optional - push a DNS server to the client, for example if you want to use your own server for name resolution

if [ -z "$1" ];then
	echo -e "Enter peer name:"
	read peername
fi

if [ -z "$peername" ];then
        echo -e "Empty name. Exiting."
        echo "Usage: addpeer.sh peername clientip"
        exit 1;
fi

peerfind=`cat -s $configfile | grep "PeerName $peername"`

if [ -n "$peerfind" ];then
	echo "Peer name $peername already exists!"
	exit 1
fi


if [ -z "$2" ];then
        echo -e "Enter client ip address"
        read clientip
fi

ipfind=`cat -s $configfile | grep AllowedIPs | grep $clientip`

if [ -n "$ipfind" ];then
        echo "IP address $ipfind already allocated!"
        exit 1
fi

if [ -z "$clientip" ];then
        echo -e "Empty client ip address. Exiting."
        echo "Usage: addpeer.sh peername clientip"
        exit 1;
fi


wg genkey | tee $peername-wgclient-private.key | wg pubkey > $peername-wgclient-public.key

cl_privatekey=`cat $peername-wgclient-private.key`
cl_pubkey=`cat $peername-wgclient-public.key`

srv_pubkey=`cat wg-public.key`
psk=`wg genpsk`
echo "$psk" > $peername-preshared-key.key

echo "
#$peername
[Interface]
Address = $clientip
PrivateKey = $cl_privatekey
#DNS = $dns

[Peer]
PublicKey = $srv_pubkey
Endpoint = $domainname:$port
AllowedIPs = 0.0.0.0/0
PresharedKey = $psk
" > $peername-client.conf

echo "
#PeerName $peername
[Peer]
PublicKey = $cl_pubkey
AllowedIPs = $clientip
PresharedKey = $psk
#EndPeername $peername

" >> wg0.conf

echo "
#PeerName $peername
[Peer]
PublicKey = $cl_pubkey
AllowedIPs = $clientip/32
PresharedKey = $psk
#EndPeername $peername
" > $peername-serversection.conf

chmod 600 $peername-*

wg set wg0 peer $cl_pubkey allowed-ips $clientip preshared-key $peername-preshared-key.key

qrencode -t ansiutf8 < $peername-client.conf