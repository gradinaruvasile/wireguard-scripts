#!/bin/bash
configfile="wg0.conf"
echo "Config file name is wg0.conf, interface is wg0. Change this in the script, first line if you need."

if [ -f $configfile ];then
	echo "Config file found."
else
	echo "Default Config file wg0.conf not found. Change this in the script, first line if it is different. Exiting."
	exit 1.
fi

peername=$1

if [ -z "$1" ];then
        echo -e "Enter peer name to delete:"
        read peername
fi

peerfind=`cat -s $configfile | grep "#PeerName $peername"`

if [ -z "$peerfind" ];then
	echo "Peer name $peername not found."
	exit 1
else
	peer_id=`sed -n "/PeerName $peername/,/EndPeerName $peername/p" $configfile |grep PublicKey |cut -d " " -f 3`
		if [ -z "$peer_id" ];then
	 		echo "Peer with Public Key $peer_id not found!"
			exit 1
		else
			echo "Removing peer $peername with Public Key $peer_id..."
			echo "Backing up config file..."
			cp $configfile $configfile-bak
			
			echo "Removing peer from running interface config..."
			
			runningtest=`wg show | grep $peer_id`
			
			if [ -z "$runningtest" ];then
				echo "Peer does not exist in running config!"
#				exit 1
			else
	                       wg set wg0 peer $peer_id remove
              
	                        if [ $? -eq 0 ];then
        	                        echo "Succesful command."
                	        else
                        	        echo "wg remove command failed. Aborting."
                                	exit 1
                        	fi

			fi
			
			
			echo "Removing peer section from $configfile..."
			
			peerverify=`cat -s $configfile | grep "#PeerName $peername"`
			
			if [ -z "$peerverify" ];then
				echo "Section not found in config file."
			else
				sed -i "/#PeerName $peername/,/#EndPeerName $peername/d" $configfile
			fi
		fi
		
		echo "removing peer files..."
		rm $peername*.*
fi
