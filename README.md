# Wireguard scripts

Some really simple scripts to help a bit with wireguard setup.
A human readable display name is used for each peer (it is done via comment tags in the config file).
Note that here are some assumptions made like the default config file is wg0.conf
Read the scripts if you have something different.

*These scripts must be in the same directory where you have your WG config file*

So far these scripts are created:

addpeer.sh - it adds new peers - it asks for a display name and ip address. It will 
create 
- private+public keys
- config files for mobile clients
- qr code for mobile agents - needs the qrencode package
- insert the peer in the working WG interface run-time. Note that the destination ip addresses (in the server config) are by default /32 so for anything else you should edit the script.

Basically you just "addpeer.sh name ipaddress" will do the following automatically,
if you read the qr code with a mobile client it should work instantly.

delpeer.sh - removes existing peers. You specify the existing peer name and it 
will be:

- removes from the WG config file and any config files created
- removes the peer from the working WG interface runtime

showpeer.sh - shows the existing peers, very simple grep against the wg config file.